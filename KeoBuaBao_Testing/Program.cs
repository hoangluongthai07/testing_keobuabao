﻿using System;

namespace TestingKeoBuaBao  
{
    // Định nghĩa enum Choice biểu diễn lựa chọn người chơi
    public enum Choice
    {
        Keo,
        Bua,
        Bao
    }

    // Class đại diện một người chơi
    public class Player
    {
        public Choice MakeChoice()
        {
            Console.WriteLine("Lựa chọn của bạn: ");
            Console.WriteLine("1 - Kéo, 2 - Búa, 3 - Bao");

            int choice;
            while (!int.TryParse(Console.ReadLine(), out choice) || choice < 1 || choice > 3)
            {
                Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                Console.WriteLine("1 - Kéo, 2 - Búa, 3 - Bao");
            }

            return (Choice)choice - 1; // Chuyển đổi từ số nguyên sang enum
        }
    }

    // Class chứa logic trò chơi
    public class GameLogic
    {
        public int DetermineWinner(Choice player1Choice, Choice player2Choice)
        {
            // Kiểm tra tính hợp lệ của lựa chọn
            if (!Enum.IsDefined(typeof(Choice), player1Choice) || !Enum.IsDefined(typeof(Choice), player2Choice))
            {
                throw new ArgumentException("Lựa chọn không hợp lệ. Lựa chọn phải là Keo, Bua hoặc Bao.");
            }

            // So sánh lựa chọn và xác định người chiến thắng
            if (player1Choice == player2Choice)
            {
                return 0; // Hòa
            }
            else if ((player1Choice == Choice.Keo && player2Choice == Choice.Bua) ||
                    (player1Choice == Choice.Bua && player2Choice == Choice.Bao) ||
                    (player1Choice == Choice.Bao && player2Choice == Choice.Keo))
            {
                return 2; // Người chơi 2 chiến thắng
            }
            else
            {
                return 1; // Người chơi 1 chiến thắng
            }
        }
    }

    // Class chứa main program 
    public class Program
    {
        public static void Main()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Trò chơi Kéo Búa Bao!");

            // Tạo hai người chơi
            Player player1 = new Player();
            Player player2 = new Player();

            // Người chơi 1 đưa ra lựa chọn
            Choice player1Choice = player1.MakeChoice();

            // Người chơi 2 đưa ra lựa chọn
            Choice player2Choice = player2.MakeChoice();

            // Tạo đối tượng chứa logic của trò chơi
            GameLogic gameLogic = new GameLogic();

            // Tính toán người chiến thắng
            int result = gameLogic.DetermineWinner(player1Choice, player2Choice);

            // In kết quả
            switch (result)
            {
                case 0:
                    Console.WriteLine("Kết quả: Hòa!");
                    break;
                case 1:
                    Console.WriteLine("Kết quả: Người chơi 1 chiến thắng!");
                    break;
                case 2:
                    Console.WriteLine("Kết quả: Người chơi 2 chiến thắng!");
                    break;
            }
        }
    }


   
}
