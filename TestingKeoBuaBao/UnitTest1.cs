namespace TestingKeoBuaBao
{
    public class GameLogicTests
    {
        [Fact]
        public void DetermineWinner_Player1Wins()
        {
            // Arrange
            var gameLogic = new GameLogic();
            var result = gameLogic.DetermineWinner(Choice.Keo, Choice.Bao);

            // Assert
            Assert.Equal(1, result);
        }

        [Fact]
        public void DetermineWinner_Player2Wins()
        {
            // Arrange
            var gameLogic = new GameLogic();
            var result = gameLogic.DetermineWinner(Choice.Bua, Choice.Bao);

            // Assert
            Assert.Equal(2, result);
        }

        [Fact]
        public void DetermineWinner_Draw()
        {
            // Arrange
            var gameLogic = new GameLogic();
            var result = gameLogic.DetermineWinner(Choice.Bua, Choice.Bua);

            // Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public void DetermineWinner_InvalidChoiceThrowsException()
        {
            // Arrange
            var gameLogic = new GameLogic();

            // Act and Assert
            Assert.Throws<ArgumentException>(() => gameLogic.DetermineWinner((Choice)10, Choice.Bua));
        }
    }
}